module.exports = {
    "name": "payment-details",
    "title": "payment-details",
    "slug": "payment-details",
    "organization": {
        "name": "payment-details",
        "owner": {
            "email": "fabio.oshiro@gmail.com"
        }
    },
    "public-key": "042376da-388b-4f39-83df-2ce21aa1a901",
    "ame-super-app-client": "1.4.4",
    "ame-miniapp-components": "1.4.4",
    "version": "0.1.0"
}
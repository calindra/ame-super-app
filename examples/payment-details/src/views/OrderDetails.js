import Ame from 'ame-super-app-client'

export default class OrderDetails {

    state = {
        orderDetails: ''
    }

    componentDidMount() {
        this.showOrderDetails()
    }

    showOrderDetails = async () => {
        let orderDetails = await Ame.getOrderDetails()
        this.setState({ orderDetails })
    }

}

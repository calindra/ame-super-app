import Ame from 'ame-super-app-client'

export default class PaymentView {
    startPayment = () => {
        let item = { name: 'my product', price: 1.99 }

        // o valor tem que ser em centavos, ou seja, 1.99 vira 199
        let itemPrice = parseInt(item.price * 100, 10)
        let paymentOrder = {
            title: 'Titulo',
            description: 'Descricao',
            // walletToken: '10f952ae-d418-4088-bdd9-20a910fafb6d', //opcional, somente para miniapps multi wallet
            amount: itemPrice, // o valor tem que ser em centavos, ou seja, 1.99 vira 199
            cashbackAmount: 0, // o valor tem que ser em centavos, ou seja, 1.99 vira 199
            customPayload: {
                cartId:
                    '12345' /* Dados customizados para fechar o pedido apos pagamento */,
            },
            hasOrderDetails: true, // opcional, somente para miniapps com suporte a ver detalhes de pedido. Ler mais na seção "Exibição de detalhes do pedido"
            items: [
                {
                    description: item.name,
                    quantity: 1,
                    amount: itemPrice,
                },
            ],
        }

        // abre a tela de pagamento da Ame
        Ame.startPayment(paymentOrder)
    }
}

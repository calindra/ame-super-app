# Payment Details

Exemplo de como utilizar os detalhes do pedido.

Para desenvolver a tela de OrderDetails rode:  
`ame-app-tools order-details uuid-do-pagamento`

Obs.: é necessário que você publique o seu mini app para ver o fluxo completo  
`ame-app-tools publish`

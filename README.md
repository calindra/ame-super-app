# AME SUPER APP
Este repositório é um Issue Tracker utilizado para manter o registro de desafios e soluções encontrados pelos desenvolvedores de miniapps para o superapp da AME



# Antes de abrir uma issue verifique se nossa documentação pode ajuda-lo
https://developer.ame.calindra.com.br/



# Para abrir uma Issue, esteja atento às recomendações abaixo:
1. Clique em **ISSUES > NOVA ISSUE** ou acesse (https://gitlab.com/calindra/ame-super-app/-/issues)
2. Selecione um dos templates de Issue que melhor se adapte ao seu caso no campo **DESCRIPTION**
3. Preencha com o máximo de detalhes possível a descrição de sua Issue informando os dados solicitados



# ATENÇÃO:
- Se você possuir prints ou telas do problema, erro ou bug em questão é possível anexa-las através da opção *ANEXAR UM ARQUIVO*
- Se for necessário fornecer dados sensíveis como TOKENS ou CREDENCIAIS DE ACESSO, não esqueça de marcar o campo *PRIVADO* logo abaixo da descrição do seu Issue.
- Não esqueça de fornecer os meios de contato mais adequados para que seja possível que nossa equipe retorne caso necessário.





